*VERSION 1.0	01/11/2018*


**Requerement:**

-Python3
-Sha256 / MD5 from the site which the file was downloaded from.

**Installation:**

not required.


**What is "trol":**

Trol is a script to extract the hashsum (sha256 or md5) from a file and compare it to the orginal one. 


**trol or trol_beta:**

-trol is the main file, fully fuctional.

-trol_beta is the beta file where new features will be added in before being added to trol.py.



**How to run:**

Go to directory where you put the trol.py file, open terminal in this folder (or cd to the folder), copy and paste the following line:

>> python trol.py

add the file you want to extract the hash and insert the sha256/md5 you've got from the website you've download the file.

Done!


*Contact:*

devlwkyan@protonmail.com

