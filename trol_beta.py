

'''                                              Trol


The objetive of this program is to extract the hashsum md5 or sha256 from a file, transforme it to hexdecimal
and out put it as a string, ask the user to input the hashsum from the website which it was downloaded and
output whether they match or not.



'''

import hashlib
import time
import sys
from termcolor import colored, cprint


test = int(
    input("\nEnter the code \"-1\" to end or any other integer to continue:  "))

while test != -1:

    # Asks for input to hashpair sha256 or md5
    hash_type = str(input("Sha256, sha512 or md5? :")).lower()

    if hash_type == "sha256":  # If input is sha256

        # This first part will get the file, or access it using typed directory
        # and will save it for extration of sha256sum.

        extract_from_file = input("Add file, or path to file\n")
        hasher = hashlib.sha256()  # Hash to sha256
        time.sleep(0.5)
        # Print Wait... and blink (termcolor module)
        cprint("Wait...", attrs=['blink'])

        with open(extract_from_file, 'rb') as open_file:
            content = open_file.read()
            hasher.update(content)
        from_extract = hasher.hexdigest()
        # Transfome the hexadecimal to string and put in lowercase
        to_string = str(from_extract).lower()
        print(from_extract)

        from_site = str(input("\nEnter the code from website\n").lower())

        if from_site == to_string:
            cprint("\n ✔ They match", "green")
        else:
            cprint("\n ✘ ATTENTION! THEY DO NOT MATCH! Your file may be corrupted, or you may have entered the wrong checksum", "red")

    elif hash_type == "md5":  # If input is md5

        # This first part will get the file, or access it using typed directory
        # and will save it for extration of md56sum.

        extract_from_file = input(
            "Drag and drop file, or enter the file path to extract the hash\n")
        hasher = hashlib.md5()  # Hash to md5
        time.sleep(0.5)
        # Print Wait... and blink (termcolor module)
        cprint("Wait...", attrs=['blink'])

        with open(extract_from_file, 'rb') as open_file:
            content = open_file.read()
            hasher.update(content)
        from_extract = hasher.hexdigest()
        # Transfome the hexadecimal to string and put in lowercase
        to_string = str(from_extract).lower()
        print(from_extract)

        from_site = str(input("\nEnter the code from website\n").lower())

    elif hash_type == "sha512":
        extract_from_file = input(
            "Drag and drop file, or enter the file path to extract the hash\n")
        hasher = hashlib.sha512()
        time.sleep(0.5)
        cprint("Wait...", attrs=["blink"])

        with open(extract_from_file, 'rb') as open_file:
            content = open_file.read()
            hasher.update(content)
        from_extract = hasher.hexdigest()
        to_string = str(from_extract).lower()
        print(from_extract)

        from_site = str(input("\nEnter the code from website\n").lower())


        if from_site == to_string:
            cprint("\nThey match", "green")
        else:
            cprint("\n ✘ ATTENTION! THEY DO NOT MATCH! Your file may be corrupted, or you may have entered the wrong checksum", "red")

    # Bool to verify whether it's True that the input is -1, if so it ends, else, repeats the loop.
    test = int(
        input("\nEnter the code \"-1\" to end or any other integer to continue:  "))
